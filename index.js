const express = require('express');
const morgan = require('morgan');

const app = express();

// json request parser
app.use(express.json());

// HTTP request logger
app.use(morgan('combined'));

// routers caller
require('./app/routes/main.js')(app);

// unknown route handler
app.use('*', (req, res, next) => {
  res.status(404).json({ error: { code: 404, message: 'Not Found' } });
});

const PORT = 3000;
app.listen(PORT, () => {
  console.log(`Server is running at http://localhost:${PORT}`);
});
