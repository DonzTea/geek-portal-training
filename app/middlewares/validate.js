const { validationResult } = require('express-validator');

/**
 * running all validation chain, promise will return json errors while any error
 * @param {ValidationChain} validations // array of validation chain from express-validator middleware
 * @returns {Promise}
 */
const validate = (validations) => {
  return async (req, res, next) => {
    await Promise.all(validations.map((validation) => validation.run(req)));

    const errors = validationResult(req);
    if (errors.isEmpty()) {
      return next();
    }

    return res.status(400).json({ errors: errors.array() });
  };
};

module.exports = validate;
