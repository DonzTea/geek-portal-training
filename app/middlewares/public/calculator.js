const { body } = require('express-validator');

const validate = require('../validate.js');

// req.body.numbers expected to be an array of number
const validateNumbers = validate([
  body('numbers')
    // must be exist
    .exists({ checkNull: true, checkFalsy: true })
    .withMessage('is not exist')
    .bail()
    // is an array
    .custom((value) => Array.isArray(value))
    .withMessage('is not an array'),
  // is array of number
  body('numbers.*').isNumeric().withMessage('is not a number').toFloat(),
]);

// req.body.numerator and req.body.denominator expected to be a number
const validateNumeratorAndDenumerator = validate([
  body('numerator')
    // is exist
    .exists({ checkNull: true, checkFalsy: true })
    .withMessage('is not exist')
    .bail()
    // is a number
    .isNumeric()
    .withMessage('is not a number')
    .toFloat(),
  body('denominator')
    // is exist
    .exists({ checkNull: true, checkFalsy: true })
    .withMessage('is not exist')
    .bail()
    // is a number
    .isNumeric()
    .withMessage('is not a number')
    .toFloat(),
]);

module.exports = {
  validateNumbers,
  validateNumeratorAndDenumerator,
};
