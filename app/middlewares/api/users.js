const { body, param } = require('express-validator');

const validate = require('../validate.js');
const usersUtils = require('../../utils/api/users.js');

const validateUserData = validate([
  body('fullName')
    .exists({ checkNull: true, checkFalsy: true })
    .withMessage('is not exist')
    .bail()
    .trim()
    .isString()
    .withMessage('is not a string'),
  body('email')
    .exists({ checkNull: true, checkFalsy: true })
    .withMessage('is not exist')
    .bail()
    .trim()
    .isEmail()
    .withMessage('is not an email')
    .normalizeEmail(),
  body('birthDate')
    .optional()
    .isDate()
    .withMessage('is not a date')
    .bail()
    .custom((value) => usersUtils.formatDateToISO8601(value))
    .isISO8601({ strict: true })
    .isBefore(new Date().toString())
    .withMessage('is not a valid date')
    .bail()
    // is 17 years old or older
    .custom(async (value, { req }) => {
      const ageConstraint = 17;
      const thisDate = new Date();
      const thisYear = thisDate.getFullYear();
      const thisMonth = thisDate.getMonth();
      const thisDay = thisDate.getDate();
      const dateConstraint = new Date(
        thisYear - ageConstraint,
        thisMonth,
        thisDay,
      ).toDateString();

      await body('birthDate')
        .isBefore(dateConstraint)
        .withMessage(`must be at least ${ageConstraint} years old`)
        .run(req);
    }),
  body('address').optional().trim().isString().withMessage('is not a string'),
  body('postalCode')
    .optional()
    .isPostalCode('any')
    .withMessage('is not a postal code'),
  body('jobTitle').optional().trim().isString().withMessage('is not a string'),
  body('phoneNumber')
    .optional()
    .isMobilePhone('any')
    .withMessage('is not a mobile phone number'),
]);

const validateParamsId = validate([
  param('id')
    .exists({ checkNull: true, checkFalsy: true })
    .withMessage('is not exist')
    .bail()
    .trim()
    .isUUID()
    .withMessage('is not an UUID'),
]);

module.exports = {
  validateUserData,
  validateParamsId,
};
