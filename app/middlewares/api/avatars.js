const multer = require('multer');

const uploadImage = (req, res, next) => {
  const storage = multer.memoryStorage();
  const upload = multer({ storage: storage }).single('image');
  upload(req, res, (err) => {
    if (err) {
      console.error(error);
      return res
        .status(500)
        .json({ error: { code: 500, message: err.message } });
    }
    next();
  });
};

const validateFile = (req, res, next) => {
  if (req.file) {
    const { mimetype } = req.file;
    if (mimetype.startsWith('image')) {
      return next();
    }
    return res
      .status(400)
      .json({ error: { code: 400, message: 'File is not an image file.' } });
  } else {
    res.status(400).json({
      error: { code: 400, message: 'Cannot read mimetype of uploaded file.' },
    });
  }
};

const validateMimetype = (req, res, next) => {
  const allowedMimetypes = ['image/jpeg', 'image/png'];
  if (req.file && !allowedMimetypes.includes(req.file.mimetype)) {
    return res.status(400).json({
      error: {
        code: 400,
        message: 'Only .jpeg and .png image files are allowed.',
      },
    });
  }
  next();
};

module.exports = {
  uploadImage,
  validateFile,
  validateMimetype,
};
