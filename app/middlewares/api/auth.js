const { body } = require('express-validator');

const validate = require('../validate.js');

const validateUsername = body('username')
  .exists({ checkFalsy: true, checkNull: true })
  .withMessage('is not exist')
  .bail()
  .isString()
  .withMessage('is not a string')
  .bail()
  .isLength({ min: 4 })
  .withMessage('is expected to have at least 4 characters or more')
  .bail()
  .isAlphanumeric()
  .withMessage('is containing illegal character');

const validatePassword = body('password')
  .exists({ checkFalsy: true, checkNull: true })
  .withMessage('is not exist')
  .bail()
  .isString()
  .withMessage('is not a string')
  .bail()
  .isLength({ min: 8 })
  .withMessage('is expected to have at least 8 characters or more');

const validatePasswordConfirmation = body('passwordConfirmation')
  .exists({ checkFalsy: true, checkNull: true })
  .withMessage('is not exist')
  .bail()
  .isString()
  .withMessage('is not a string')
  .bail()
  .custom((value, { req }) => {
    if (value !== req.body.password) {
      throw new Error('password confirmation does not match password');
    }
    return value;
  });

const validateRegisterInput = validate([
  validateUsername,
  validatePassword,
  validatePasswordConfirmation,
]);

const validateLoginInput = validate([validateUsername, validatePassword]);

module.exports = {
  validateRegisterInput,
  validateLoginInput,
};
