const express = require('express');

const router = express.Router();

const imagesMiddleware = require('../../middlewares/api/avatars.js');
const imagesController = require('../../controllers/api/avatars.js');

router
  .post(
    '/',
    imagesMiddleware.uploadImage,
    imagesMiddleware.validateFile,
    imagesMiddleware.validateMimetype,
    imagesController.create,
  )
  .get('/:fileName', imagesController.read);

module.exports = router;
