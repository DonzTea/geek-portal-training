const express = require('express');

const authController = require('../../controllers/api/auth.js');
const authMiddleware = require('../../middlewares/api/auth.js');

const router = express.Router();

router
  .post(
    '/register',
    authMiddleware.validateRegisterInput,
    authController.register,
  )
  .post('/login', authMiddleware.validateLoginInput, authController.login);

module.exports = router;
