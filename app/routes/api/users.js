const express = require('express');

const usersMiddleware = require('../../middlewares/api/users.js');
const usersController = require('../../controllers/api/users.js');

const router = express.Router();

router
  .post('/', usersMiddleware.validateUserData, usersController.create)
  .get('/', usersController.read)
  .put(
    '/:id',
    usersMiddleware.validateParamsId,
    usersMiddleware.validateUserData,
    usersController.update,
  )
  .delete('/:id', usersMiddleware.validateParamsId, usersController.destroy);

module.exports = router;
