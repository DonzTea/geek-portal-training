module.exports = (app) => {
  // public routes
  const calculatorRouter = require('./public/calculator.js');

  app.use('/calculator', calculatorRouter);

  // api routes
  const usersApiRouter = require('./api/users.js');
  const avatarsApiRouter = require('./api/avatars.js');
  const authApiRouter = require('./api/auth.js');

  app.use('/api/users', usersApiRouter);
  app.use('/api/avatars', avatarsApiRouter);
  app.use('/api/auth', authApiRouter);
};
