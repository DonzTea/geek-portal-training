const express = require('express');

const calculatorMiddleware = require('../../middlewares/public/calculator.js');
const calculatorController = require('../../controllers/public/calculator.js');

const router = express.Router();

router
  .get('/', calculatorController.index)
  .post(
    '/tambah',
    calculatorMiddleware.validateNumbers,
    calculatorController.addition,
  )
  .post(
    '/kurang',
    calculatorMiddleware.validateNumbers,
    calculatorController.subtraction,
  )
  .post(
    '/kali',
    calculatorMiddleware.validateNumbers,
    calculatorController.multiplication,
  )
  .post(
    '/bagi',
    calculatorMiddleware.validateNumeratorAndDenumerator,
    calculatorController.division,
  );

module.exports = router;
