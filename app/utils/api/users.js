const faker = require('faker/locale/id_ID');

const usersData = require('../../data/api/users.js');

const generateRandomId = () => {
  const id = faker.random.uuid();
  return id;
};

const generateRandomFullName = () => {
  const fullName = faker.name.findName();
  return fullName;
};

const generateRandomEmail = (fullName) => {
  let email;
  if (fullName.length > 0) {
    const splittedName = fullName.split(' ');
    const [firstName, lastName] = splittedName;
    email = faker.internet.email(firstName, lastName).toLowerCase();
  } else {
    email = faker.internet.email().toLowerCase();
  }
  return email;
};

const generateRandomJobTitle = () => {
  const jobTitle = faker.name.jobTitle();
  return jobTitle;
};

const formatDateToISO8601 = (dateString) => {
  const formattedDate = new Date(dateString).toISOString().slice(0, 10);
  return formattedDate;
};

const generateRandomBirthDate = () => {
  const birthDate = faker.date.between('1975-01-01', '1998-12-31');
  const formattedBirthDate = formatDateToISO8601(birthDate);
  return formattedBirthDate;
};

const generateRandomAddress = () => {
  const address = faker.address.streetAddress(true);
  return address;
};

const generateRandomPostalCode = () => {
  const postalCode = faker.address.zipCode();
  return postalCode;
};

const generateRandomPhoneNumber = () => {
  const phoneNumberPrefixes = usersData.phoneNumberPrefixes;
  const selectedPhoneNumberPrefix =
    phoneNumberPrefixes[Math.floor(Math.random() * phoneNumberPrefixes.length)];
  const phoneNumberFormat = selectedPhoneNumberPrefix + '########';
  const phoneNumber = faker.phone.phoneNumber(phoneNumberFormat);
  return phoneNumber;
};

const generateRandomUser = () => {
  const id = generateRandomId();
  const fullName = generateRandomFullName();
  const email = generateRandomEmail(fullName);
  const birthDate = generateRandomBirthDate();
  const address = generateRandomAddress();
  const postalCode = generateRandomPostalCode();
  const jobTitle = generateRandomJobTitle();
  const phoneNumber = generateRandomPhoneNumber();
  const user = {
    id,
    fullName,
    email,
    birthDate,
    address,
    postalCode,
    jobTitle,
    phoneNumber,
  };
  return user;
};

module.exports = {
  generateRandomId,
  formatDateToISO8601,
  generateRandomUser,
};
