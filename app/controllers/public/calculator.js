const index = (req, res) => {
  res.status(200).send('calculator page');
};

const addition = (req, res) => {
  const { numbers } = req.body;
  const totalSum = numbers.reduce((acc, curr) => acc + curr, 0);
  res.status(200).json({ data: { input: numbers, output: totalSum } });
};

const subtraction = (req, res) => {
  const { numbers } = req.body;
  const totalSubtraction = numbers.reduce((acc, curr) => acc - curr);
  res.status(200).json({ data: { input: numbers, output: totalSubtraction } });
};

const multiplication = (req, res) => {
  const { numbers } = req.body;
  const totalMultiplication = numbers.reduce((acc, curr) => acc * curr, 1);
  res
    .status(200)
    .json({ data: { input: numbers, output: totalMultiplication } });
};

const division = (req, res) => {
  const { numerator, denominator } = req.body;
  const divisionResult = numerator / denominator;
  res.status(200).json({
    data: {
      numerator,
      denominator,
      output:
        divisionResult === Infinity
          ? "error: can't divide by zero"
          : divisionResult,
    },
  });
};

module.exports = {
  index,
  addition,
  subtraction,
  multiplication,
  division,
};
