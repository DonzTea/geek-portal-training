const fs = require('fs');
const path = require('path');

const sharp = require('sharp');

const create = async (req, res) => {
  try {
    const file = req.file;
    const newFileName = Date.now() + '.png';

    // avatar setup
    const width = 100;
    const height = 100;
    const roundedCorners = Buffer.from(
      `<svg><rect x="0" y="0" width="${width}" height="${height}" rx="${Math.round(
        width / 2,
      )}" ry="${Math.round(height / 2)}"/></svg>`,
    );

    // create rounded corner avatar image
    await sharp(file.buffer)
      .resize(width, height)
      .composite([
        {
          input: roundedCorners,
          blend: 'dest-in',
        },
      ])
      .png()
      .toFile(path.join(__dirname, `../../public/images/${newFileName}`));

    file.newname = newFileName;
    delete file.buffer;
    return res.status(201).json({ data: file });
  } catch (error) {
    console.error(error);
    res
      .status(500)
      .json({ error: { code: 500, message: 'Internal Server Error' } });
  }
};

const read = async (req, res) => {
  const { fileName } = req.params;
  const avatarImageAbsolutePath = path.join(
    __dirname,
    `../../public/images/${fileName}`,
  );

  // check if file exists
  fs.stat(avatarImageAbsolutePath, (err) => {
    // if not exist return error response
    if (err) {
      console.error(err);
      return res
        .status(500)
        .json({ error: { code: 500, message: err.message } });
    }

    res.status(200).sendFile(avatarImageAbsolutePath);
  });
};

module.exports = {
  create,
  read,
};
