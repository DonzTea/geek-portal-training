const { generateRandomId } = require('../../utils/api/users.js');

const register = (req, res) => {
  const id = generateRandomId();
  const { username, password, passwordConfirmation } = req.body;
  const account = {
    id,
    username,
    password,
    passwordConfirmation,
  };
  res.status(201).json({ data: account });
};

const login = (req, res) => {
  const id = generateRandomId();
  const { username, password } = req.body;
  const account = {
    id,
    username,
    password,
  };
  res.status(200).json({ data: account });
};

module.exports = {
  register,
  login,
};
