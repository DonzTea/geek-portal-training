const path = require('path');

const usersUtils = require('../../utils/api/users.js');

const create = (req, res) => {
  const id = usersUtils.generateRandomId();
  const {
    fullName,
    email,
    birthDate,
    address,
    postalCode,
    jobTitle,
    phoneNumber,
  } = req.body;

  const user = {
    id,
    fullName,
    email,
    birthDate: usersUtils.formatDateToISO8601(birthDate),
    address,
    postalCode,
    jobTitle,
    phoneNumber,
  };
  res.status(201).json({ data: user });
};

const read = (req, res) => {
  const users = Array(5)
    .fill()
    .map(() => usersUtils.generateRandomUser());
  res.status(200).json({ data: users });
};

const update = (req, res) => {
  const id = req.params.id;
  const {
    fullName,
    email,
    birthDate,
    address,
    postalCode,
    jobTitle,
    phoneNumber,
  } = req.body;

  const updatedUser = {
    id,
    fullName,
    email,
    birthDate: usersUtils.formatDateToISO8601(birthDate),
    address,
    postalCode,
    jobTitle,
    phoneNumber,
  };
  res.status(200).json({ data: updatedUser });
};

const destroy = (req, res) => {
  const id = req.params.id;
  const user = usersUtils.generateRandomUser();
  user.id = id;
  res.status(200).json({ data: user });
};

module.exports = {
  create,
  read,
  update,
  destroy,
};
